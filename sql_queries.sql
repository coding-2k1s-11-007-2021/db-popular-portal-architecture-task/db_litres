SELECT
    sum(price)
FROM Customers c
LEFT JOIN Customers_Books cb ON c.id = cb.customer_id
LEFT JOIN Books b ON cb.book_id = b.id
WHERE :date_ordered_start <= date_ordered AND date_ordered <= :date_ordered_end;

SELECT
    *
FROM Books b
WHERE title = :title or author = :author or rate >= :rate;

SELECT
    *
FROM Books b
LEFT JOIN Genres g on b.genre_id = g.id
WHERE g.name = :genre_name;

SELECT
    *
FROM Customers
WHERE email = :email;

SELECT
    *
FROM Books as b
LEFT JOIN Customers_Books cb on b.id = cb.book_id
LEFT JOIN Customers as c on c.id = cb.customer_id
WHERE customer_id = :customer_id;

SELECT
    *
FROM cart
WHERE customer_id = :customer_id;

SELECT
    title, genre
FROM most_purchased_books
WHERE genre = :genre;

SELECT
    *
FROM most_rate_books
WHERE genre = :genre;

SELECT *
FROM amount_for_the_current_month;

SELECT
    authority_level = 1 AS is_manager,
    authority_level = 2 AS is_admin
FROM Customers
WHERE id = :customer_id;

SELECT
    b.title AS book_name,
    c.name AS customer_name,
    cbr.rate AS customer_rate_for_book,
    cbr.description AS customer_description_for_book
FROM Customers_Books_Rates cbr
LEFT JOIN Customers c on c.id = cbr.customer_id
LEFT JOIN Books b on b.id = cbr.book_id
WHERE b.id = :book_id;
