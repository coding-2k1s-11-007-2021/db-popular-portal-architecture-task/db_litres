-- Вывести рейсы с запланированной датой прилёта 01.08.2017 в аэропорты Москвы со статусом Arrived.
-- Для каждого рейса вывести поле, говорящее, заканчивался ли этот полёт в первой половине дня (включая ровно 12:00)
-- или во второй (вывести значение “before midday” или “after midday”).
-- {flight_id, flight_no, departure_airport, arrival_airport, scheduled_departure, scheduled_arrival, status, before_after_midday}

EXPLAIN (ANALYZE)
WITH filtered_flights AS (
    SELECT
        flight_id
    FROM flights f
    WHERE scheduled_arrival::DATE = '2017-08-01' AND status = 'Arrived'
), filtered_airports AS (
    SELECT
        airport_code
    FROM airports a
    WHERE a.city = 'Moscow'
)
SELECT
    f.flight_id,
    f.flight_no,
    f.departure_airport,
    f.arrival_airport,
    f.scheduled_departure,
    f.scheduled_arrival,
    f.status,
    CASE
        WHEN f.scheduled_arrival::TIME <= '12:00' THEN 'before midday'
        ELSE 'after midday'
    END AS before_after_midday
FROM filtered_flights ff
LEFT JOIN flights f ON ff.flight_id = f.flight_id
LEFT JOIN filtered_airports fa ON f.arrival_airport = fa.airport_code;

-- C помощью CTE удалось заранее отфильтровать полеты и аэропорты.
-- Таким образом, операции стали выполняться быстрее за счет меньшего количества данных.
-- Отключение nested loops привело к увеличению времени запроса, т.к. вместо него используются медленные hash join и дополнительный seq scan.
-- Индексы создавать не для чего, т.к. flights уже имеют необходимый и достаточный индекс, а airports - это view.
-- С work_mem все в порядке, временные файлы на диске не используются.
