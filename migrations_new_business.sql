begin transaction;
ALTER TABLE Books DROP COLUMN genre_id;
CREATE TABLE Books_Genres
(
   book_id INT NOT NULL,
   genre_id INT NOT NULL,
   CONSTRAINT book_id_fk FOREIGN KEY (book_id) REFERENCES Books (id) ON DELETE CASCADE,
   CONSTRAINT genre_id_fk FOREIGN KEY (genre_id) REFERENCES Genres (id) ON DELETE CASCADE,
   PRIMARY KEY (book_id, genre_id)
);
commit transaction;
