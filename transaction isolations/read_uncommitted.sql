-- На этом уровне нам не важна точность данных, это мы можем использовать для обработки неважной статистики, например
-- вывод общего количества проданных билетов на сайте. Тут вот например мы обновляем данные о продажах, но случилась
-- ошибка и произошел ролбэк. Для транзакции 2 не важны точные данные, так что все ок.


--transaction 1;
BEGIN;
UPDATE tickets SET status=’paid’ WHERE id=1243;

-- transaction 2;
SELECT count(id) from tickets WHERE status=’paid’;

UPDATE tickets SET status=’paaaid’ WHERE id=1245;
ROLLBACK WORK;
