begin transaction;
CREATE TABLE Customers
(
    id bigserial not null PRIMARY KEY,
    name varchar(100) NOT NULL,
    card varchar(100) unique,
    password varchar(300) NOT NULL,
    email varchar(100) UNIQUE NOT NULL,
    balance int NOT NULL CHECK (balance >= 0),
    authority_level int NOT NULL CHECK (authority_level in (0, 1, 2)) -- 0 - обычный пользователь, 1 - менеджер, 2 - админ
);

-- Пользователи будут авторизовываться с помощью емаила, поэтому важно его быстро находить
CREATE INDEX customers_email_idx ON Customers USING HASH (email); -- Тип Hash, потому что email уникальный

CREATE TABLE Genres
(
    id bigserial not null PRIMARY KEY,
    name varchar(100) UNIQUE NOT NULL
);
CREATE TABLE Books
(
    id bigserial not null PRIMARY KEY,
    author VARCHAR(100),
    price int NOT NULL CHECK (price > 0),
    rate float CHECK (rate IS NULL OR rate >= 0),
    title varchar(100) NOT NULL,
    description text,
    link_to_book varchar(200) unique NOT NULL,
    genre_id INT,
    CONSTRAINT book_genre_id_fk FOREIGN KEY (genre_id) REFERENCES Genres (id) ON DELETE SET NULL
);

-- Пользователи часто ищут книги через поиск, и вбивают туда они зачастую именно название.
CREATE INDEX books_title_idx ON Books (title); -- Тип B-tree, потому что title не уникальный
-- Если пользователь не ищет конкретную книгу, а просто зашел посмотреть, что есть, то часто он будет сортировать книги по их рейтингу или выбирать жанр.
CREATE INDEX books_rate_idx ON Books (rate); -- Тип B-tree, потому что rate не уникальный
CREATE INDEX genres_name_idx ON Genres USING HASH (name); -- Тип Hash, потому что name уникальный

CREATE TABLE Cart
(
    customer_id INT NOT NULL,
    book_id INT NOT NULL,
    no_in_queue INT NOT NULL,
    CONSTRAINT cart_pk PRIMARY KEY (customer_id, book_id),
    CONSTRAINT cart_customer_id_fk FOREIGN KEY (customer_id) REFERENCES Customers (id) ON DELETE CASCADE,
    CONSTRAINT cart_book_id_fk FOREIGN KEY (book_id) REFERENCES Books (id) ON DELETE CASCADE
);
CREATE TABLE Customers_Books
(
    customer_id INT NOT NULL,
    book_id INT NOT NULL,
    date_ordered date NOT NULL,
    CONSTRAINT customers_books_pk PRIMARY KEY (customer_id, book_id),
    CONSTRAINT customers_books_customer_id_fk FOREIGN KEY (customer_id) REFERENCES Customers (id) ON DELETE CASCADE,
    CONSTRAINT customers_books_book_id_fk FOREIGN KEY (book_id) REFERENCES Books (id) ON DELETE CASCADE
);

CREATE VIEW most_purchased_books AS(
    SELECT
    COUNT(b.id) as count,
    b.title as title, g.name as genre
    FROM books as b
        LEFT JOIN genres g on b.genre_id = g.id
        RIGHT JOIN cart c on b.id = c.book_id
    GROUP BY genre, title
    ORDER BY count DESC);

CREATE VIEW most_rate_books AS (
    SELECT
        title, g.name as genre, rate
    FROM books
        LEFT JOIN genres g on g.id = books.genre_id
    WHERE rate > 4.5
    ORDER BY rate DESC);

CREATE MATERIALIZED VIEW amount_for_the_current_month AS (
    SELECT
    sum(price)
    FROM Customers c
        LEFT JOIN Customers_Books cb ON c.id = cb.customer_id
        LEFT JOIN Books b ON cb.book_id = b.id
    WHERE date_part('year', date_ordered) = date_part('year', current_date)
            and date_part('month', date_ordered) = date_part('month', current_date)
);

CREATE TABLE Customers_Books_Rates
(
    customer_id INT,
    book_id INT NOT NULL,
    rate FLOAT NOT NULL CHECK (rate >= 0 AND rate <= 5),
    description TEXT,
    CONSTRAINT customers_books_rates_pk PRIMARY KEY (customer_id, book_id),
    CONSTRAINT customers_books_rates_customer_id_fk FOREIGN KEY (customer_id) REFERENCES Customers (id) ON DELETE SET NULL,
    CONSTRAINT customers_books_rates_book_id_fk FOREIGN KEY (book_id) REFERENCES Books (id) ON DELETE CASCADE
);
commit transaction;
